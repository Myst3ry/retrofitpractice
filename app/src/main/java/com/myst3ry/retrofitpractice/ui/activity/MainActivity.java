package com.myst3ry.retrofitpractice.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.annotation.WorkerThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.myst3ry.retrofitpractice.ForecastIntentService;
import com.myst3ry.retrofitpractice.IntentConstants;
import com.myst3ry.retrofitpractice.OnForecastItemClickListener;
import com.myst3ry.retrofitpractice.R;
import com.myst3ry.retrofitpractice.RetrofitPracticeApp;
import com.myst3ry.retrofitpractice.data.local.DBManager;
import com.myst3ry.retrofitpractice.data.local.model.WeatherDailyForecast;
import com.myst3ry.retrofitpractice.ui.adapter.ForecastAdapter;
import com.myst3ry.retrofitpractice.utils.LinearSpacingItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity {

    @BindView(R.id.weather_rec_view)
    RecyclerView mWeatherRecyclerView;
    @BindView(R.id.refresher)
    SwipeRefreshLayout mRefresher;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private ForecastAdapter mAdapter;
    private ForecastBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prepareHandler();

        initAdapter();
        initRecyclerView();
        initReceiver();
        setRefreshListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
        requestForecasts();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    private void prepareHandler() {
        mHandler = new Handler(getMainLooper());
    }

    private void initAdapter() {
        mAdapter = new ForecastAdapter(new ForecastItemClickListenerImpl());
    }

    private void initRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mWeatherRecyclerView.setLayoutManager(layoutManager);
        mWeatherRecyclerView.addItemDecoration(LinearSpacingItemDecoration.newBuilder()
                .spacing(getResources().getDimensionPixelSize(R.dimen.margin_half))
                .orientation(LinearLayoutManager.VERTICAL)
                .includeEdge(true)
                .build());
        mWeatherRecyclerView.setAdapter(mAdapter);
    }

    private void initReceiver() {
        mReceiver = new ForecastBroadcastReceiver();
        mIntentFilter = new IntentFilter(IntentConstants.ACTION_FORECASTS_RESULT);
    }

    private void setRefreshListener() {
        mRefresher.setOnRefreshListener(this::startForecastService);
    }

    private void requestForecasts() {
        ((RetrofitPracticeApp)getApplication()).submitSingleTask(this::getForecastsFromDb);
    }

    @WorkerThread
    private void getForecastsFromDb() {
        final DBManager manager = DBManager.getInstance(this);
        final List<WeatherDailyForecast> forecasts = manager.getWeatherForecasts();
        mHandler.post(() -> updateUI(forecasts));
    }

    private void updateUI(final List<WeatherDailyForecast> forecasts) {
        hideProgressBar();
        hideRefresher();
        if (forecasts != null && !forecasts.isEmpty()) {
            mAdapter.setForecastList(forecasts);
        } else {
            showProgressBar();
            startForecastService();
        }
    }

    private void startForecastService() {
        startService(ForecastIntentService.newForecastsIntent(this));
    }

    private void showProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.GONE) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void hideRefresher() {
        if (mRefresher != null && mRefresher.isRefreshing()) {
            mRefresher.setRefreshing(false);
        }
    }

    private final class ForecastItemClickListenerImpl implements OnForecastItemClickListener {

        @Override
        public void onWeatherItemClick(final int epochDate) {
            startActivity(ForecastDetailActivity.newExtraIntent(MainActivity.this, epochDate));
        }
    }

    private final class ForecastBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (IntentConstants.ACTION_FORECASTS_RESULT.equalsIgnoreCase(intent.getAction())) {
                requestForecasts();
            }
        }
    }
}
