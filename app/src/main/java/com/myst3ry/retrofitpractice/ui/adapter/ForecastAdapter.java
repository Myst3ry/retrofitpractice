package com.myst3ry.retrofitpractice.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.retrofitpractice.utils.DateUtils;
import com.myst3ry.retrofitpractice.OnForecastItemClickListener;
import com.myst3ry.retrofitpractice.R;
import com.myst3ry.retrofitpractice.utils.StringUtils;
import com.myst3ry.retrofitpractice.data.local.model.WeatherDailyForecast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.WeatherViewHolder> {

    private final OnForecastItemClickListener mListener;
    private List<WeatherDailyForecast> mForecastList;

    public ForecastAdapter(final OnForecastItemClickListener listener) {
        this.mForecastList = new ArrayList<>();
        this.mListener = listener;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WeatherViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forecast, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        final WeatherDailyForecast forecast = getWeatherDailyForecast(position);

        holder.mCity.setText(forecast.getCity());
        holder.mWeekday.setText(DateUtils.getDayOfWeek(forecast.getEpochDate()));
        holder.mDate.setText(DateUtils.parseDate(forecast.getDate()));
        holder.mAvgTemperature.setText(StringUtils.getAvgTempString(forecast.getTemperatureMin(),
                forecast.getTemperatureMax(), forecast.getUnit()));
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    public void setForecastList(final List<WeatherDailyForecast> forecasts) {
        this.mForecastList = forecasts;
        notifyDataSetChanged();
    }

    private WeatherDailyForecast getWeatherDailyForecast(final int position) {
        return mForecastList.get(position);
    }

    final class WeatherViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.city)
        TextView mCity;
        @BindView(R.id.weekday)
        TextView mWeekday;
        @BindView(R.id.date)
        TextView mDate;
        @BindView(R.id.avg_temp)
        TextView mAvgTemperature;

        @OnClick(R.id.weather_container)
        public void onClick() {
            final WeatherDailyForecast forecast = getWeatherDailyForecast(getLayoutPosition());
            mListener.onWeatherItemClick(forecast.getEpochDate());
        }

        WeatherViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
