package com.myst3ry.retrofitpractice.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.myst3ry.retrofitpractice.IntentConstants;
import com.myst3ry.retrofitpractice.R;
import com.myst3ry.retrofitpractice.RetrofitPracticeApp;
import com.myst3ry.retrofitpractice.data.local.DBManager;
import com.myst3ry.retrofitpractice.data.local.model.WeatherDailyForecast;
import com.myst3ry.retrofitpractice.utils.DateUtils;
import com.myst3ry.retrofitpractice.utils.StringUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ForecastDetailActivity extends AppCompatActivity {

    @BindView(R.id.forecast_city)
    TextView mCityText;
    @BindView(R.id.forecast_day_of_week)
    TextView mWeekdayText;
    @BindView(R.id.forecast_date)
    TextView mDateText;
    @BindView(R.id.forecast_max_temp)
    TextView mMaxTempText;
    @BindView(R.id.forecast_day_phrase)
    TextView mDayPhraseText;
    @BindView(R.id.forecast_min_temp)
    TextView mMinTempText;
    @BindView(R.id.forecast_night_phrase)
    TextView mNightPhraseText;

    private int mEpochDate;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        prepareHandler();
        getCurrentForecastDate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(DateUtils.getDayOfWeek(mEpochDate));
        requestDailyForecast(mEpochDate);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareHandler() {
        mHandler = new Handler(getMainLooper());
    }

    private void getCurrentForecastDate() {
        mEpochDate = getIntent().getIntExtra(IntentConstants.EXTRA_EPOCH_DATE, 0);
    }

    private void requestDailyForecast(final int date) {
        ((RetrofitPracticeApp)getApplication()).submitSingleTask(() -> getDailyForecastFromDb(date));
    }

    @WorkerThread
    private void getDailyForecastFromDb(final int date) {
        final DBManager manager = DBManager.getInstance(this);
        final WeatherDailyForecast forecast = manager.getDailyForecast(date);
        mHandler.post(() -> setWeatherForecastInfo(forecast));
    }

    private void setWeatherForecastInfo(final WeatherDailyForecast forecast) {
        mCityText.setText(forecast.getCity());
        mWeekdayText.setText(DateUtils.getDayOfWeek(forecast.getEpochDate()));
        mDateText.setText(DateUtils.parseDate(forecast.getDate()));
        mMaxTempText.setText(StringUtils.getTempUnitString(forecast.getTemperatureMax(), forecast.getUnit()));
        mMinTempText.setText(StringUtils.getTempUnitString(forecast.getTemperatureMin(), forecast.getUnit()));
        mDayPhraseText.setText(forecast.getDayPhrase());
        mNightPhraseText.setText(forecast.getNightPhrase());
    }

    public static Intent newExtraIntent(final Context context, final int epochDate) {
        final Intent intent = newIntent(context);
        intent.putExtra(IntentConstants.EXTRA_EPOCH_DATE, epochDate);
        return intent;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, ForecastDetailActivity.class);
    }
}
