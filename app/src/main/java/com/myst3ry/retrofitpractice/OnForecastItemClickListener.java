package com.myst3ry.retrofitpractice;

public interface OnForecastItemClickListener {

    void onWeatherItemClick(final int epochDate);
}
