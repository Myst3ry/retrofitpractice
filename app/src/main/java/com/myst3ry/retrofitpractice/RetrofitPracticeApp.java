package com.myst3ry.retrofitpractice;

import android.app.Application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class RetrofitPracticeApp extends Application {

    private ExecutorService mDBExecutorService;

    @Override
    public void onCreate() {
        super.onCreate();
        mDBExecutorService = Executors.newSingleThreadExecutor();
    }

    public void submitSingleTask(final Runnable task) {
        if (mDBExecutorService != null) {
            mDBExecutorService.submit(task);
        }
    }
}