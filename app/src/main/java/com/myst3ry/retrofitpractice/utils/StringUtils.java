package com.myst3ry.retrofitpractice.utils;

import java.util.Locale;

public final class StringUtils {

    public static String getAvgTempString(final int min, final int max, final String unit) {
        return String.format("%s - %s", getTempUnitString(min, unit), getTempUnitString(max, unit));
    }

    public static String getTempUnitString(final int temp, final String unit) {
        return String.format(Locale.getDefault(), "%d%s", temp, unit);
    }

    private StringUtils() { }
}
