package com.myst3ry.retrofitpractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class DailyForecast {

	@SerializedName("Temperature")
	private Temperature temperature;

	@SerializedName("Night")
	private Night night;

	@SerializedName("EpochDate")
	private int epochDate;

	@SerializedName("Day")
	private Day day;

	@SerializedName("Date")
	private String date;

	public Temperature getTemperature(){
		return temperature;
	}

	public Night getNight(){
		return night;
	}

	public int getEpochDate(){
		return epochDate;
	}

	public Day getDay(){
		return day;
	}

	public String getDate(){
		return date;
	}
}