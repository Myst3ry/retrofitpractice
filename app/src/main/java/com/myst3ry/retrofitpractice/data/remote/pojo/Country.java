package com.myst3ry.retrofitpractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class Country{

	@SerializedName("LocalizedName")
	private String localizedName;

	@SerializedName("EnglishName")
	private String englishName;

	public String getLocalizedName(){
		return localizedName;
	}

	public String getEnglishName(){
		return englishName;
	}
}