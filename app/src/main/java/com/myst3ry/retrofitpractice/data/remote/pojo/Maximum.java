package com.myst3ry.retrofitpractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class Maximum{

	@SerializedName("Value")
	private float value;

	@SerializedName("Unit")
	private String unit;

	public float getValue(){
		return value;
	}

	public String getUnit(){
		return unit;
	}
}