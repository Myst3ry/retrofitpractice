package com.myst3ry.retrofitpractice.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitHelper {

    public static final int FORECASTS_API_VERSION = 1;
    public static final int LOCATIONS_API_VERSION = 1;
    private static final String BASE_URL = "http://dataservice.accuweather.com/";

    public WeatherApi getService() {

        final Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(WeatherApi.class);
    }
}
