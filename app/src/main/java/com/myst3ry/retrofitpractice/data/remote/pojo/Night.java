package com.myst3ry.retrofitpractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class Night{

	@SerializedName("IconPhrase")
	private String iconPhrase;

	@SerializedName("Icon")
	private int icon;

	public String getIconPhrase(){
		return iconPhrase;
	}

	public int getIcon(){
		return icon;
	}
}