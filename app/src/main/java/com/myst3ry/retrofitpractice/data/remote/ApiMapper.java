package com.myst3ry.retrofitpractice.data.remote;

import android.util.Log;

import com.myst3ry.retrofitpractice.data.remote.pojo.City;
import com.myst3ry.retrofitpractice.data.remote.pojo.DailyForecast;
import com.myst3ry.retrofitpractice.data.remote.pojo.ForecastResponse;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

public final class ApiMapper {

    private static final String API_KEY = "tnp335Rq4a55DLlKlJryVOa9jbhHKGce";
    private static final String TAG = "ApiMapper";

    private RetrofitHelper mHelper;

    private static volatile ApiMapper INSTANCE;

    public static ApiMapper getInstance(final RetrofitHelper helper) {
        ApiMapper instance = INSTANCE;
        if (instance == null) {
            synchronized (ApiMapper.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new ApiMapper(helper);
                }
            }
        }
        return instance;
    }

    private ApiMapper(final RetrofitHelper helper) {
        this.mHelper = helper;
    }

    public List<City> getCitiesByQuery(final String cityQuery, final String language) {
        Response<List<City>> response = null;
        try {
            response = mHelper.getService()
                    .getCitiesByQuery(RetrofitHelper.LOCATIONS_API_VERSION, API_KEY, cityQuery, language)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        }

        if (response != null) {
            if (response.isSuccessful()) {
                return response.body();
            } else {
                Log.e(TAG, response.code() + " " + response.message());
            }
        }

        return null;
    }

    public List<DailyForecast> getWeatherForecastByLocation(final String locationKey, final String language, final boolean metric) {
        Response<ForecastResponse> response = null;
        try {
            response = mHelper.getService().getWeatherForecast(RetrofitHelper.FORECASTS_API_VERSION, locationKey,
                    API_KEY, language, metric).execute();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        }

        if (response != null) {
            if (response.isSuccessful()) {
                return Objects.requireNonNull(response.body()).getDailyForecasts();
            } else {
                Log.e(TAG, response.code() + " " + response.message());
            }
        }

        return null;
    }
}
