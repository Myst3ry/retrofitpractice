package com.myst3ry.retrofitpractice.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.myst3ry.retrofitpractice.data.local.model.WeatherDailyForecast;

import java.util.ArrayList;
import java.util.List;

public final class DBManager {

    private static final String TAG = "DBManager";
    private static final String DATE_WHERE_CLAUSE = "epoch_date = ?";

    private final DBHelper mDBHelper;
    private SQLiteDatabase mDatabase;

    private static volatile DBManager INSTANCE;

    public static DBManager getInstance(final Context context) {
        DBManager instance = INSTANCE;
        if (instance == null) {
            synchronized (DBManager.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new DBManager(context);
                }
            }
        }
        return instance;
    }

    private DBManager(final Context context) {
        mDBHelper = new DBHelper(context);
    }

    public List<WeatherDailyForecast> getWeatherForecasts() {
        List<WeatherDailyForecast> forecasts = null;
        try {
            mDatabase = mDBHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final Cursor cursor = mDatabase.query(DBContract.FORECASTS_TABLE, null, null,
                    null, null, null, DBContract.COLUMN_EPOCH_DATE + " ASC");
            forecasts = parseForecastList(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
        return forecasts;
    }

    public WeatherDailyForecast getDailyForecast(final int epochDate) {
        WeatherDailyForecast currentForecast = null;
        try {
            mDatabase = mDBHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final String date = String.valueOf(epochDate);
            final Cursor cursor = mDatabase.query(DBContract.FORECASTS_TABLE, null, DATE_WHERE_CLAUSE,
                    new String[]{date}, null, null, null);

            currentForecast = parseForecastCursor(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
        return currentForecast;
    }

    public void removeWeatherForecasts() {
        try {
            mDatabase = mDBHelper.getWritableDatabase();
            mDatabase.beginTransaction();

            removeForecastsInternal();

            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    public void saveWeatherForecasts(final List<WeatherDailyForecast> forecasts) {
        for (final WeatherDailyForecast forecast : forecasts) {
            insertWeatherDailyForecast(forecast);
        }
    }

    private void insertWeatherDailyForecast(final WeatherDailyForecast forecast) {
        try {
            mDatabase = mDBHelper.getWritableDatabase();
            final ContentValues values = getWeatherForecastCV(forecast);
            mDatabase.beginTransaction();

            insertForecastInternal(values);

            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    private void insertForecastInternal(final ContentValues values) {
        if (mDatabase != null) {
            mDatabase.insert(DBContract.FORECASTS_TABLE, null, values);
        }
    }

    private void removeForecastsInternal() {
        if (mDatabase != null) {
            mDatabase.delete(DBContract.FORECASTS_TABLE, null, null);
        }
    }

    private ContentValues getWeatherForecastCV(final WeatherDailyForecast forecast) {
        final int forecastEpochDate = forecast.getEpochDate();
        final String forecastDate = forecast.getDate();
        final int forecastTempMin = forecast.getTemperatureMin();
        final int forecastTempMax = forecast.getTemperatureMax();
        final String forecastTempUnit = forecast.getUnit();
        final String forecastDayPhrase = forecast.getDayPhrase();
        final String forecastNightPhrase = forecast.getNightPhrase();
        final String forecastCity = forecast.getCity();

        final ContentValues forecastValues = new ContentValues();

        forecastValues.put(DBContract.COLUMN_EPOCH_DATE, forecastEpochDate);
        forecastValues.put(DBContract.COLUMN_DATE, forecastDate);
        forecastValues.put(DBContract.COLUMN_TEMP_MIN, forecastTempMin);
        forecastValues.put(DBContract.COLUMN_TEMP_MAX, forecastTempMax);
        forecastValues.put(DBContract.COLUMN_TEMP_UNIT, forecastTempUnit);
        forecastValues.put(DBContract.COLUMN_DAY_PHRASE, forecastDayPhrase);
        forecastValues.put(DBContract.COLUMN_NIGHT_PHRASE, forecastNightPhrase);
        forecastValues.put(DBContract.COLUMN_CITY, forecastCity);

        return forecastValues;
    }

    private WeatherDailyForecast parseForecastCursor(final Cursor cursor) {
        WeatherDailyForecast forecast = null;
        if (cursor.moveToFirst()) {
            forecast = getForecastFromCursor(cursor);
        }
        return forecast;
    }

    private List<WeatherDailyForecast> parseForecastList(final Cursor cursor) {
        List<WeatherDailyForecast> forecasts = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                forecasts.add(getForecastFromCursor(cursor));
                cursor.moveToNext();
            }
        }
        return forecasts;
    }

    private WeatherDailyForecast getForecastFromCursor(final Cursor cursor) {
        final int epochDate = cursor.getInt(cursor.getColumnIndex(DBContract.COLUMN_EPOCH_DATE));
        final int minTemp = cursor.getInt(cursor.getColumnIndex(DBContract.COLUMN_TEMP_MIN));
        final int maxTemp = cursor.getInt(cursor.getColumnIndex(DBContract.COLUMN_TEMP_MAX));
        final String unit = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_TEMP_UNIT));
        final String city = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_CITY));
        final String date = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_DATE));
        final String dayPhrase = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_DAY_PHRASE));
        final String nightPhrase = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_NIGHT_PHRASE));

        return new WeatherDailyForecast(epochDate, date, maxTemp, minTemp, unit, dayPhrase, nightPhrase, city);
    }


}
