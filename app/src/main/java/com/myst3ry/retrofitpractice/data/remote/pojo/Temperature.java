package com.myst3ry.retrofitpractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class Temperature{

	@SerializedName("Minimum")
	private Minimum minimum;

	@SerializedName("Maximum")
	private Maximum maximum;

	public Minimum getMinimum(){
		return minimum;
	}

	public Maximum getMaximum(){
		return maximum;
	}
}