package com.myst3ry.retrofitpractice;

public final class IntentConstants {

    public static final String ACTION_GET_FORECASTS = BuildConfig.APPLICATION_ID + "action.FORECASTS";
    public static final String ACTION_FORECASTS_RESULT = BuildConfig.APPLICATION_ID + "action.FORECASTS_RESULT";

    public static final String EXTRA_EPOCH_DATE = BuildConfig.APPLICATION_ID + "extra.EPOCH_DATE";

    private IntentConstants() { }
}
