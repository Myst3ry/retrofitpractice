package com.myst3ry.retrofitpractice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

import com.myst3ry.retrofitpractice.data.local.DBManager;
import com.myst3ry.retrofitpractice.data.remote.pojo.City;
import com.myst3ry.retrofitpractice.data.remote.pojo.DailyForecast;
import com.myst3ry.retrofitpractice.data.local.model.WeatherDailyForecast;
import com.myst3ry.retrofitpractice.data.remote.ApiMapper;
import com.myst3ry.retrofitpractice.data.remote.RetrofitHelper;

import java.util.ArrayList;
import java.util.List;

public final class ForecastIntentService extends IntentService {

    private static final String SERVICE_NAME = "ForecastIntentService";
    private List<WeatherDailyForecast> mForecastList;
    private ApiMapper mApiMapper;

    private String mCountry, mCity, mLanguage;
    private boolean mIsMetric;

    public ForecastIntentService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApiMapper();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (IntentConstants.ACTION_GET_FORECASTS.equalsIgnoreCase(action)) {
                initResources();
                performForecastsRequest();
                if (mForecastList != null) {
                    saveWeatherForecasts(mForecastList);
                }
            }
        }
    }

    private void initApiMapper() {
        mApiMapper = ApiMapper.getInstance(new RetrofitHelper());
    }

    private void initResources() {
        final Resources resources = getResources();
        mCountry = resources.getString(R.string.country);
        mCity = resources.getString(R.string.city);
        mLanguage = resources.getString(R.string.language);
        mIsMetric = resources.getBoolean(R.bool.metrics);
    }

    private void performForecastsRequest() {
        final String locationKey = getLocationKey();
        if (locationKey != null && !locationKey.isEmpty()) {
            getForecastsByLocation(locationKey);
        }
    }

    private String getLocationKey() {
        final List<City> cities = mApiMapper.getCitiesByQuery(mCity, mLanguage);

        String locationKey = null;
        if (cities != null) {
            for (final City city : cities) {
                if (city.getCountry().getLocalizedName().equals(mCountry)) {
                    locationKey = city.getKey();
                }
            }
        } else {
            stopSelf();
        }
        return locationKey;
    }

    private void getForecastsByLocation(final String locationKey) {
        final List<DailyForecast> initialForecasts = mApiMapper.getWeatherForecastByLocation(locationKey,
                mLanguage, mIsMetric);

        if (initialForecasts != null) {
            mForecastList = mapForecasts(initialForecasts);
        } else {
            stopSelf();
        }
    }

    private List<WeatherDailyForecast> mapForecasts(final List<DailyForecast> forecasts) {
        final List<WeatherDailyForecast> weatherDailyForecastList = new ArrayList<>();
        for (final DailyForecast forecast : forecasts) {
            weatherDailyForecastList.add(mapForecast(forecast));
        }
        return weatherDailyForecastList;
    }

    private WeatherDailyForecast mapForecast(final DailyForecast dailyForecast) {
        return new WeatherDailyForecast(
                dailyForecast.getEpochDate(),
                dailyForecast.getDate(),
                Math.round(dailyForecast.getTemperature().getMaximum().getValue()),
                Math.round(dailyForecast.getTemperature().getMinimum().getValue()),
                dailyForecast.getTemperature().getMinimum().getUnit(),
                dailyForecast.getDay().getIconPhrase(),
                dailyForecast.getNight().getIconPhrase(),
                mCity);
    }

    private void saveWeatherForecasts(final List<WeatherDailyForecast> forecasts) {
        final DBManager manager = DBManager.getInstance(getApplicationContext());
        manager.removeWeatherForecasts();
        manager.saveWeatherForecasts(forecasts);
        sendBroadcast();
    }

    private void sendBroadcast() {
        final Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(IntentConstants.ACTION_FORECASTS_RESULT);
        sendBroadcast(broadcastIntent);
    }

    public static Intent newForecastsIntent(final Context context) {
        final Intent intent = newIntent(context);
        intent.setAction(IntentConstants.ACTION_GET_FORECASTS);
        return intent;
    }

    private static Intent newIntent(final Context context) {
        return new Intent(context, ForecastIntentService.class);
    }
}
